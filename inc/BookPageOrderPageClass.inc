<?php

class BookPageOrderPageClass
{
    // book ID
    public $bid = NULL;
    
	// menu ID
    public $mlid = NULL;

    // method declaration
    public function displayBid() {
        echo $this->bid;
    }
    
    public function displayMlid() {
        echo $this->mlid;
    }
    
	function __construct($nid) {
		$get_bid = db_select('book' , 'bk')
		->condition('bk.nid' , $nid)
		->fields('bk' , array('bid'))
		->execute()
		->fetchField();
		// return $bid;
		$this->bid = $get_bid;
		
		// todo: optimize combine with above query
		$get_mlid = db_select('book' , 'bk')
		->condition('bk.nid' , $nid)
		->fields('bk' , array('mlid'))
		->execute()
		->fetchField();
		$this->mlid = $get_mlid;
		
	}
	
	public function bpo_book_toc ($depth = 99) {
		return book_toc($this->bid, $depth);
	}
	
	public function book_page_order_is_last_page () {	
		//$page = new BookPageOrderPageClass($nid);
		$current_toc = $this->bpo_book_toc(99);
		end($current_toc);
		$last_mlid = key($current_toc);
		return ($this->mlid == $last_mlid);
	}
}

