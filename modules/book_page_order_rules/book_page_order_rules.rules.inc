<?php

/**
* Implementation of hook_rules_action_info().
* @ingroup rules
*/
function book_page_order_rules_rules_action_info() {
	return array(
	'book_page_order_rules_record_page_view' => array(
      'group' => t('Book Page Order'),
      'label' => t('Record this page view and update next allowed page'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Current user uid'), 
        	'wrap' => FALSE,
        	),
     	),
    ),
    'book_page_order_rules_mark_book_complete' => array(
      'group' => t('Book Page Order'),
      'label' => t('Mark book complete'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Current user uid'), 
        	'wrap' => FALSE,
        	),
    	),
    ),
    'book_page_order_rules_redirect_to_next_allowed_page' => array(
      'group' => t('Book Page Order'),
      'label' => t('Redirect user to next allowed page'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Current user uid'), 
        	'wrap' => FALSE,
        	),
      	),
    ),
    'book_page_order_rules_rebuild_history' => array(
      'group' => t('Book Page Order'),
      'label' => t('Reset TOC in user history for this book'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Current user uid'), 
        	'wrap' => FALSE,
        	),
      	),
    ),
  );
}

/**
* Implementation of hook_rules_condition_info().
*/
function book_page_order_rules_rules_condition_info() {
  return array(
    'book_page_order_rules_is_page_next_allowed' => array(
      'group' => 'Book page order',
      'label' => t('Is this the next allowed page in this book'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Current user uid'), 
        	'wrap' => FALSE,
        	),
      	),
      'module' => 'book_page_order_rules',
    ),
    'book_page_order_rules_is_last_page' => array(
      'group' => 'Book page order',
      'label' => t('Is this the last page in this book'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
      	),
      'module' => 'book_page_order_rules',
    ),
	'book_page_order_rules_verify_book_order' => array(
      'group' => 'Book page order',
      'label' => t('Is history TOC the same as book TOC'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the Current user uid'), 
        	'wrap' => FALSE,
        	),
      	),
      'module' => 'book_page_order_rules',
    ),
    'book_page_order_rules_is_book_complete' => array(
      'group' => 'Book page order',
      'label' => t('Has this book been previously completed'),
      'arguments' => array(
        'nid' => array(
        	'type' => 'integer', 
        	'label' => t('Select the current Book Page node nid'),
        	'wrap' => FALSE,
        	),
        'uid' => array(
        	'type' => 'integer', 
        	'label' => t('Return the Current user uid'), 
        	'wrap' => FALSE,
        	),
      	),
      'module' => 'book_page_order_rules',
    ),
  );
}

/** -- Actions -- */

/**
 * book_page_order_rules_record_page_view();
 * Record this page as having been viewed and update
 * the next available page property for this book in
 * $user_history
 */
function book_page_order_rules_record_page_view($nid, $uid) {
	$page = new BookPageOrderPageClass($nid);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	$user_history[$page->bid][$page->mlid] = TRUE;
	book_page_order_rules_update_next_allowed_page($user_history, $page->bid, $user);
	book_order_page_save_user_history($user_history, $user);
}

/**
 * book_page_order_rules_mark_book_complete()
 * Action - forward to the next allowed page
 */
function book_page_order_rules_mark_book_complete($nid, $uid) {
	$page = new BookPageOrderPageClass($nid);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	$user_history[$page->bid]['#complete'] = TRUE;
	book_order_page_save_user_history($user_history, $user);	
}

/**
 * book_page_order_rules_redirect_to_next_allowed_page()
 * Action - forward to the next allowed page
 */
function book_page_order_rules_redirect_to_next_allowed_page($nid, $uid) {	
	$page = new BookPageOrderPageClass($nid);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	book_page_order_rules_goto_next_allowed_page($user_history, $page->bid);
}

/**
 * book_page_order_rules_rebuild_history()
 * action - rebuild the TOC in the user's history for this book
 */
function book_page_order_rules_rebuild_history($nid, $uid) {
	$page = new BookPageOrderPageClass($nid);
	$current_toc = $page->bpo_book_toc(99);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	unset($user_history[$page->bid]);
	$user_history[$page->bid] = $current_toc;
	book_order_page_save_user_history ($user_history, $user);
	drupal_set_message(t('This book has bee edited. You must start from the begenning in order to complete this course. Sorry for the trouble.'));
	// book_page_order_rules_goto_next_allowed_page($user_history, $page->bid);
}

/** -- Conditions -- */

/**
 * book_page_order_rules_is_page_next_allowed()
 * Condition - is this the next allowed page?
 */
function book_page_order_rules_is_page_next_allowed ($nid, $uid) {
	$page = new BookPageOrderPageClass($nid);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	if($user_history[$page->bid]['#next_allowed_mlid'] == NULL) {
		return TRUE;
	}
	return ($user_history[$page->bid]['#next_allowed_mlid'] == $page->mlid);
}

/**
 * book_page_order_rules_is_last_page()
 * Condition - is this the last page in the book?
 */
function book_page_order_rules_is_last_page ($nid) {	
	$page = new BookPageOrderPageClass($nid);
	$current_toc = $page->bpo_book_toc(99);
	end($current_toc);
	$last_mlid = key($current_toc);
	return ($page->mlid == $last_mlid);
}

/**
 * book_page_order_rules_verify_book_order()
 * condition - Does the book outline in $user_history match the current book outline?
 */
function book_page_order_rules_verify_book_order ($nid, $uid) {
	$page = new BookPageOrderPageClass($nid);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	$current_toc = $page->bpo_book_toc(99);
	book_page_order_rules_build_page_history_index($current_toc);
	return book_page_order_rules_match_keys($current_toc, $user_history[$page->bid]);
}

/**
 * book_page_order_rules_is_book_complete()
 * condition - has this member finished this book previously?
 */
function book_page_order_rules_is_book_complete ($nid, $uid) {
	$page = new BookPageOrderPageClass($nid);
	$user = user_load($uid);
	$user_history = book_page_order_rules_init_user_book_history ($user, $page->bid);
	return array_key_exists ('#complete', $user_history[$page->bid]);
}

/** -- Helper Functions -- */

/**
 * book_page_order_rules_init_user_history ()
 * Load up the $user_history array and return it. Create an empty
 * history and save it to the user record if not.
 */
function book_page_order_rules_init_user_history ($user) {
	$user_history = array();
	if (isset($user->book_page_order_rules_history['und'][0]['value'])) {
		$user_history_json = $user->book_page_order_rules_history['und'][0]['value'];
		$user_history = json_decode($user_history_json, TRUE);
	} else {
		book_order_page_save_user_history($user_history, $user);
	}
	return $user_history;
}

/**
 * book_order_page_save_user_history()
 * Save the supplied user history array to the 
 * history field of the user record.
 *
 */
function book_order_page_save_user_history ($user_history, $user) {
	$user_history_json = json_encode($user_history);
	$edit = array(
	  'book_page_order_rules_history' => array(
		'und' => array(
		  0 => array(
			'value' => $user_history_json,
		  ),
		),
	  ),
	);
	user_save($user, $edit);
}

/**
 * book_page_order_rules_init_user_book_history ()
 * Add this book to the user history if it is not there.
 */
function book_page_order_rules_init_user_book_history ($user, $bid) {
	$user_history = book_page_order_rules_init_user_history ($user);
	if (!isset($user_history[$bid])) {
		$toc = book_toc($bid, 99);
		book_page_order_rules_build_page_history_index($toc);
		$user_history[$bid] = $toc;
		book_order_page_save_user_history ($user_history, $user);
	}
	return $user_history;
}

/**
 * book_page_order_rules_update_next_allowed_page()
 * Update and save the next allowed page after the current viewed page
 */
function book_page_order_rules_update_next_allowed_page(&$user_history, $bid, $user) {
	$next_allowed_mlid = book_page_order_rules_get_next_page($user_history[$bid]);
	$user_history[$bid]['#next_allowed_mlid'] = $next_allowed_mlid;
}

/**
 * book_page_order_rules_fetch_nid()
 *
 * Return a mlid's nid - we need to translate mlid to nid
 */
function book_page_order_rules_fetch_nid($mlid) {
	$nid = db_select('book' , 'bk')
	->condition('bk.mlid' , $mlid)
	->fields('bk' , array('nid'))
	->execute()
	->fetchField();
	return $nid;
}

/**
 * book_page_order_rules_build_page_history_index()
 * 
 * A helper function to build a current index of a book,
 * We need it in a slightly different form to track true/false
 * for a page view history.
 *
 * @param $toc
 * An array built by the function book_toc(). All values will be
 * replaced by the value of 'false'
 * 
 */
function book_page_order_rules_build_page_history_index(&$toc) {
	// array_shift2($toc);
	$count = 0;
	while(current($toc)) {
		if (key($toc) == '#next_allowed_mlid') {
			next($toc);
			continue;
		}
		if ($count == 0) {
			$toc[key($toc)] = true;
			next($toc);
			$count++;
			continue;
			}
		if ($count == 1) {
			// Page one is next in line
			$toc['#next_allowed_mlid'] = key($toc);
			}
    	$toc[key($toc)] = false;
    	$count++;
    	next($toc);
    }
   	 	
}

/**
 * book_page_order_rules_get_next_page()
 *
 * Given an array of mlid keys with boolean values
 * indicating the viewed status of each page by the user
 * what would the next, un-read page be?
 */
function book_page_order_rules_get_next_page($history) {
	foreach ($history as $key=>$value) {
		if (!$value) {
			// so pages are marked TRUE when visted, the 
			// first page that is marked false is the next page.
			return $key;
		}	
	}
}

/**
 * book_page_order_rules_match_keys()
 *
 * Do the supplied arrays differ? Returns a boolean.
 */
function book_page_order_rules_match_keys($toc, $user_history) {
	reset($user_history);
	while($element = current($toc)) {
    	if (key($toc) == key($user_history)) {
	    	next($user_history);
	   	 	next($toc);
   	 	} else {
   	 		return false;
   	 	}	
	}
	/** still here? Maybe arrays are of differing length? */
	if (!count($toc) == count($user_history)) { return false; }
	return true;
}

/**
 * book_page_order_rules_goto_next_allowed_page()
 * Redirect to the next allowed page of this book
 * in the $user_history
 */
function book_page_order_rules_goto_next_allowed_page($user_history, $bid) {
	$next_page_mlid = $user_history[$bid]['#next_allowed_mlid'];
	$next_page_nid = book_page_order_rules_fetch_nid($next_page_mlid);
	global $base_url;
	$url = $base_url . '/node/' . $next_page_nid;
	book_page_order_rules_drupal_goto ($url);
}

/**
 * book_page_order_rules_drupal_goto()
 * Redirect the Rules way - see @rules_action_drupal_goto()
 */
function book_page_order_rules_drupal_goto($url, $force = TRUE) {
  // If force is enabled, remove any destination parameter.
  if ($force && isset($_GET['destination'])) {
    unset($_GET['destination']);
  }
  // We don't invoke drupal_goto() right now, as this would end the the current
  // page execution unpredictly for modules. So we'll take over drupal_goto()
  // calls from somewhere else via hook_drupal_goto_alter() and make sure
  // a drupal_goto() is invoked before the page is output with
  // rules_page_build().
  $GLOBALS['_rules_action_drupal_goto_do'] = array($url, $force);
}