<?php

/**
 * Expose the myfield field value as a context condition.
 */
class context_condition_book_last_page extends context_condition {
  public function condition_values() {
    return array('last_page' => 'Detect the last page of a book');
  }

  function execute($node) {
    $page = new BookPageOrderPageClass($node->nid);
    $last_page = $page->book_page_order_is_last_page();
    foreach ($this->get_contexts() as $context) {
      if ($last_page == true) {
        $this->condition_met($context,$last_page);
      }
    }
  }


}